#pragma once

namespace soi
{
    // Each of car activities (being on runway, being on the bridge)
    // will take some time: sleep(rand() % (max-min)) + min
    // TimeParams stores these times (in milliseconds).
    struct TimeParams
    {
        unsigned roadwayMin;
        unsigned roadwayMax;
        unsigned bridgeMin;
        unsigned bridgeMax;

        bool isCorrect();
    };

    class Settings
    {
    public:
        Settings(const char *filename);
        void printAll();
        bool isDataCorrect();
        inline unsigned getNumberOfCars() const { return numberOfCars; }
        inline unsigned getNumberOfIterationsToEnd() const { return numberOfIterationsToEnd; }
        inline float getSideProbability() const { return sideProbability; } 
        inline const TimeParams& getTimes() const { return timeParams; }

    private:
        unsigned numberOfCars;
        unsigned numberOfIterationsToEnd; // unlike in semaphores project, this will be number of iterations FOR EACH car
        float sideProbability; // probability that car will go in DIRECTION_UP (see enum in SharedData.h)
        TimeParams timeParams;
    };
}
