#include "BridgeMonitor.h"
#include "Car.h"

soi::BridgeMonitor::BridgeMonitor()
    : Monitor(), actualDirection(DIRECTION_DOWN), numberOfCarsOnTheBridge(0),
      numberOfCarsUntilDirectionChange(MAX_NUMBER_OF_CARS_UNTIL_DIRECTION_CHANGE)
{
    numberOfCarsWaiting[0] = numberOfCarsWaiting[1] = 0;
}

void soi::BridgeMonitor::enterBridgde(soi::Car &car, soi::Direction direction)
{
    enter();
    if(direction == actualDirection)
    {
        if(numberOfCarsOnTheBridge == MAX_NUMBER_OF_CARS_ON_THE_BRIDGE ||
                (numberOfCarsUntilDirectionChange == 0 && numberOfCarsWaiting[!direction] > 0))
        {
            ++numberOfCarsWaiting[direction];
            fprintf(stderr, "Car %i stops at actual direction.\n", car.getId());
            printData();
            wait(conditionDirection[direction]);
            --numberOfCarsWaiting[direction];
        }
    }
    else if(numberOfCarsOnTheBridge > 0) // direction != actDirection
    {
        ++numberOfCarsWaiting[direction];
        fprintf(stderr, "Car %i stops at opposite direction.\n", car.getId());
        printData();
        wait(conditionDirection[direction]);
        --numberOfCarsWaiting[direction];
    }
    if(numberOfCarsWaiting[!direction] > 0)
        --numberOfCarsUntilDirectionChange;
    else
        numberOfCarsUntilDirectionChange = MAX_NUMBER_OF_CARS_UNTIL_DIRECTION_CHANGE;
    actualDirection = direction;
    ++numberOfCarsOnTheBridge;
    fprintf(stderr, "Car %i is crossing the bridge.\n", car.getId());
    printData();
    leave();
}

void soi::BridgeMonitor::leaveBridge(soi::Car &car)
{
    enter();
    --numberOfCarsOnTheBridge;
    if(numberOfCarsWaiting[actualDirection] > 0 &&
        (numberOfCarsUntilDirectionChange > 0 || numberOfCarsWaiting[!actualDirection] == 0))
    {
        fprintf(stderr, "Car %i leaves the bridge, signalling car from actual direction.\n", car.getId());
        printData();
        signal(conditionDirection[actualDirection]);
    }
    else if(numberOfCarsWaiting[!actualDirection] > 0 && numberOfCarsOnTheBridge == 0)
    {
        fprintf(stderr, "Car %i leaves the bridge, signalling car from opposite direction.\n", car.getId());
        printData();
        numberOfCarsUntilDirectionChange = MAX_NUMBER_OF_CARS_UNTIL_DIRECTION_CHANGE;
        signal(conditionDirection[!actualDirection]);
    }
    else
    {
        fprintf(stderr, "Car %i leaves the bridge, not signalling anything.\n", car.getId());
        printData();
    }
    leave();
}

void soi::BridgeMonitor::printEndMessage(soi::Car &car)
{
    enter();
    fprintf(stderr, "Car %i has finished running (iterations[0] = %u, iterations[1] = %u).\n\n",
                    car.getId(), car.getNumberOfIterationsUp(), car.getNumberOfIterationsDown());
    leave();
}

void soi::BridgeMonitor::printData()
{
    // no enter/leave - this is a private method called only inside critical section in other methods
    fprintf(stderr, "    actualDirection:                       %i\n"
                    "    numberOfCarsOnTheBridge:               %u\n"
                    "    numberOfCarsWaiting[actualDirection]:  %u\n"
                    "    numberOfCarsWaiting[!actualDirection]: %u\n"
                    "    numberOfCarsUntilDirectionChange:      %u\n\n",
                    actualDirection,
                    numberOfCarsOnTheBridge,
                    numberOfCarsWaiting[actualDirection],
                    numberOfCarsWaiting[!actualDirection],
                    numberOfCarsUntilDirectionChange);
}
