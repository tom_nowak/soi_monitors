#include "Car.h"
#include "BridgeMonitor.h"
#include <thread>
#include <chrono>

using namespace std::chrono;

std::atomic<unsigned> soi::Car::numberOfCreatedInstances(0);

soi::Car::Tests::Tests(const soi::TimeParams &tp, unsigned carId)
    : directionDistribution(0.0, 1.0),
      bridgeTimeDistribution(tp.bridgeMin, tp.bridgeMax),
      roadwayTimeDistribution(tp.roadwayMin, tp.roadwayMax)
{
    numberOfIterations[0] = numberOfIterations[1] = 0;
    auto init_seed = static_cast<std::mt19937::result_type>(system_clock::now().time_since_epoch().count());
    init_seed += static_cast<std::mt19937::result_type>(carId);
    randomGenerator.seed(init_seed);
}

soi::Car::Car(BridgeMonitor &m, const soi::Settings &s)
    : monitor(m), settings(s), carId(++numberOfCreatedInstances), tests(settings.getTimes(), carId)
{}

void soi::Car::run()
{
    for(unsigned i=0; i<settings.getNumberOfIterationsToEnd(); ++i)
    {
        Direction dir = getRandomDirection();
        ++tests.numberOfIterations[dir];
        driveOnRoadway();
        monitor.enterBridgde(*this, dir);
        crossBridge();
        monitor.leaveBridge(*this);
    }
    // the following call simply prints a short message, but it is called via monitor, so that it is atomic
    monitor.printEndMessage(*this);
}

void soi::Car::driveOnRoadway()
{
    std::this_thread::sleep_for(milliseconds(tests.roadwayTimeDistribution(tests.randomGenerator)));
}

soi::Direction soi::Car::getRandomDirection()
{
    float x = tests.directionDistribution(tests.randomGenerator);
    if(x <= settings.getSideProbability())
        return DIRECTION_UP;
    return DIRECTION_DOWN;
}

void soi::Car::crossBridge()
{
    std::this_thread::sleep_for(milliseconds(tests.bridgeTimeDistribution(tests.randomGenerator)));
}
