#include "Car.h"
#include "Settings.h"
#include "BridgeMonitor.h"
#include <cstdio>
#include <thread>
#include <vector>

int main(int argc, char **argv)
{
    using namespace soi;

    if(argc != 2)
    {
        printf("Usage: %s <path to file with settings>\n", argv[0]);
        return 1;
    }
    std::vector<std::thread> threads;
    Settings settings(argv[1]);
    BridgeMonitor monitor;
    auto lambda = [&]() { Car(monitor, settings).run(); };
    for(unsigned i = 1; i < settings.getNumberOfCars(); ++i)
        threads.emplace_back(lambda);
    lambda(); // in current thread
    for(auto &t: threads)
        t.join();
    return 0;
}
