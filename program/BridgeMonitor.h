#pragma once
#include "monitor/monitor.h"

namespace soi
{
    class Car;

    enum Direction : bool
    {
        DIRECTION_UP = 0,
        DIRECTION_DOWN = 1
    };

    class BridgeMonitor: protected Monitor
    {
    public:
        BridgeMonitor();
        void enterBridgde(soi::Car &car, soi::Direction direction);
        void leaveBridge(soi::Car &car);
        void printEndMessage(soi::Car &car);

        static const unsigned MAX_NUMBER_OF_CARS_ON_THE_BRIDGE = 2;
        static const unsigned MAX_NUMBER_OF_CARS_UNTIL_DIRECTION_CHANGE = 5;

    protected:
        Condition conditionDirection[2];
        unsigned numberOfCarsWaiting[2];
        Direction actualDirection;
        unsigned numberOfCarsOnTheBridge;
        unsigned numberOfCarsUntilDirectionChange;

        void printData(); // called inside enterBridge and leaveBridge
    };
}
