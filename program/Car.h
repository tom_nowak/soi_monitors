#pragma once
#include <random>
#include <atomic>
#include "Settings.h"
#include "BridgeMonitor.h" // include it, instead of declaring class BridgeMonitor, to get access to Direction enum

namespace soi
{
    class Car
    {
    public:
        Car(BridgeMonitor &m, const Settings &s);
        void run();
        inline unsigned getId() { return carId; }
        inline unsigned getNumberOfIterationsUp() { return tests.numberOfIterations[0]; }
        inline unsigned getNumberOfIterationsDown() { return tests.numberOfIterations[1]; }

    protected:
        // Data used in synchronization:
        BridgeMonitor &monitor;
        const Settings &settings;

        // Data used in tests (mesuring time and number of iterations for each side - up/down):
        const unsigned carId;
        struct Tests
        {
            unsigned numberOfIterations[2];
            std::mt19937 randomGenerator;
            std::uniform_real_distribution<float> directionDistribution; // to choose random direction
            std::uniform_int_distribution<unsigned> bridgeTimeDistribution, roadwayTimeDistribution; // to sleep for ...

            Tests(const TimeParams &tp, unsigned carId);
        } tests;

        // numberOfCreatedInstances is atomic, because cars are constructed in different threads.
        // It is incremented in constructor, but not decremented in destructor - it is meant only
        // to provide unique id for each car. I 'dare' to use C++ atomic instead of Monitor, because
        // counting car instances is only for testing, this is not a part of the bridge synchronization task.
        static std::atomic<unsigned> numberOfCreatedInstances;

        void driveOnRoadway();
        Direction getRandomDirection();
        void crossBridge();
    };
}
