#include "Settings.h"
#include <cstdio>
#include <cstdlib>

bool soi::TimeParams::isCorrect()
{
    return (roadwayMin <= roadwayMax) &&
           (bridgeMin <= bridgeMax);
}

soi::Settings::Settings(const char *filename)
{
#define READ(x, format) \
    if(fscanf(file, format, &x) != 1) \
    { \
        fprintf(stderr, "incorrect contents of settings file (unable to read data)\n"); \
        exit(-1); \
    } \
    for(char c = fgetc(file); c!='\n' && c!=EOF; c = fgetc(file));

    FILE *file = fopen(filename, "r");
    if(!file)
        perror("could not open settings file");
    READ(numberOfCars, "%u")
    READ(numberOfIterationsToEnd, "%u")
    READ(sideProbability, "%f")
    READ(timeParams.roadwayMin, "%u")
    READ(timeParams.roadwayMax, "%u")
    READ(timeParams.bridgeMin, "%u")
    READ(timeParams.bridgeMax, "%u")
    fclose(file);
    printAll();
    if(!isDataCorrect())
    {
        fprintf(stderr, "incorrect contents of settings file\n");
        exit(-1);
    }
#undef READ
}

void soi::Settings::printAll()
{
    fprintf(stderr, "Running program with settings:\n"
                    "   numberOfCars:                         %u\n"
                    "   numberOfIterationsToEnd:              %u\n"
                    "   sideProbability:                      %f\n"
                    "   timeParams.roadwayMin (milliseconds): %u\n"
                    "   timeParams.roadwayMax (milliseconds): %u\n"
                    "   timeParams.bridgeMin (milliseconds):  %u\n"
                    "   timeParams.bridgeMax (milliseconds):  %u\n\n",
                    numberOfCars,
                    numberOfIterationsToEnd,
                    sideProbability,
                    timeParams.roadwayMin,
                    timeParams.roadwayMax,
                    timeParams.bridgeMin,
                    timeParams.bridgeMax);
}

bool soi::Settings::isDataCorrect()
{
    return (numberOfCars > 0) &&
           (numberOfIterationsToEnd > 0) &&
           (sideProbability >= 0.0f) &&
           (sideProbability <= 1.0f) &&
           timeParams.isCorrect();
}
