Wąski most z "inteligentnymi" światłami

Po moście możliwy jest w danej chwili ruch tylko w jednym kierunku (GÓRA lub DÓŁ). Na moście jednocześnie mieszczą się tylko 2 samochody. W momencie gdy pojawia się (lub już oczekuje) samochód z kierunku przeciwnego niż obowiązujący aktualnie na moście, przez most może przejechać co najwyżej 5 samochodów z aktualnego kierunku po czym kierunek na moście jest zmieniany. Napisać monitor obsługujący ruch na moście.
